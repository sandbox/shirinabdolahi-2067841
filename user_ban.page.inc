<?php

function user_ban_list() {
  global $user;
  $uid = $user->uid;
  $flag = flag_get_flag('user_ban');
  $result = db_select('flagging')
          ->fields('flagging', array('entity_id'))
          ->condition('uid', $uid)
          ->condition('entity_type', 'user')
          ->condition('fid', $flag->fid)
          ->execute();
  $header = array(t('User'), t('Username'), t('Remove ban'));
  $rows = array();
  foreach ($result as $record) {
    $banned_user = user_load($record->entity_id);
    $rows[] = array(
        theme('user_picture', array('account' => $banned_user)),
        format_username($banned_user),
        flag_create_link('user_ban', $banned_user->uid)
    );
  }

  $build = array();
  $build[] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header
  );
  $build[] = drupal_get_form('user_ban_form');
  return $build;
}

function user_ban_form($form, &$form_state) {
  $form['user_to_ban'] = array(
      '#type' => 'textfield',
      '#title' => t('ban user'),
      '#defualt_value' => '',
      '#autocomplete' => 'ON',
      '#autocomplete_path' => 'user/autocomplete',
      '#id' => 'user_to_ban'
  );
  if (module_exists('realname')) {
    $form['user_to_ban']['#autocomplete_path'] = 'realname/autocomplete';
  }
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t("Ban")
  );
  return $form;
}

function user_ban_form_validate($form, &$form_state) {
  global $user;
  $user_to_ban_name = $form_state['values']['user_to_ban'];
  $user_to_ban = user_load_by_name($user_to_ban_name);
  if (is_object($user_to_ban)) {
    if ($user->uid == $user_to_ban->uid) {
      form_set_error('user_to_ban', t("Can not ban yourself."));
    }
    if (user_ban_exists($user, $user_to_ban)) {
      form_set_error('user_to_ban', $user_to_ban_name . t("Already banned."));
    }
  } else {
    form_set_error('user_to_ban', "Invalid username.");
  }
}

function user_ban_form_submit($form, &$form_state) {
  $user_to_ban = user_load_by_name($form_state['values']['user_to_ban']);
  drupal_set_message(t('!username has been banned.', array('!username' => format_username($user_to_ban))));
  flag('flag', 'user_ban', $user_to_ban->uid);
}
