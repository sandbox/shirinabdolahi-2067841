<?php

include_once 'user_ban.flag.inc';

/**
 * Implements hook_permission().
 */
function user_ban_permission() {
  return array(
      'bypass user ban module' => array(
          'title' => t('Bypass user ban module'),
          'description' => t('bypassing user ban access.'),
      ),
  );
}

/**
 * Implements hook_menu().
 */
function user_ban_menu() {
  $items = array();
  $items['user/%user/user-ban/list'] = array(
      'title' => t('User Ban List'),
      'page callback' => 'user_ban_list',
      'description' => 'This page is accessible to authenticated users only',
      'type' => MENU_LOCAL_TASK,
      'access callback' => 'user_is_logged_in',
      'file' => 'user_ban.page.inc',
  );
  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function user_ban_menu_alter(&$items) {
  $items['user/%user']['access callback'] = 'user_ban_user_profile_access';
  $items['user/%user']['access arguments'] = array(1);
}

function user_ban_user_profile_access($account) {
  global $user;
  if ($user->uid == $account->uid || user_access('bypass user ban module')) {
    return TRUE;
  }
  if (user_view_access($account)) {
    if (user_ban_exists($account, $user)) {
      drupal_set_message(t('You can not access to this profile. The user had been banned you.'), 'error', FALSE);
      drupal_goto('<front>');
      return FALSE;
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_FORM_ID_form_alter.
 */
function user_ban_form_comment_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  $comment_ban = FALSE;
  if (isset($form['pid']['#value']) && $parent_comment = comment_load($form['pid']['#value'])) {
    $author_uid = $parent_comment->uid;
    $comment_ban = user_ban_exists($author_uid, $user->uid);
  }

  $author_uid = $form['#node']->uid;
  $node_ban = user_ban_exists($author_uid, $user->uid);

  if (($node_ban || $comment_ban) && !user_access('bypass user ban module')) {
    $form['user-ban'] = array(
        '#markup' => t('You can not post comment on this content, the author has been banned you.'),
        '#weight' => -50,
    );
  }
}

/**
 * Implements hook_comment_presave().
 */
function user_ban_comment_presave($comment) {
  global $user;
  $comment_ban = FALSE;
  if (isset($comment->pid) && $parent_comment = comment_load($comment->pid)) {
    $author_uid = $parent_comment->uid;
    $comment_ban = user_ban_exists($author_uid, $user->uid);
  }

  $node = node_load($comment->nid);
  $author_uid = $node->uid;
  $node_ban = user_ban_exists($author_uid, $user->uid);

  if (($node_ban || $comment_ban) && !user_access('bypass user ban module')) {
    drupal_set_message(t('You can not post comment on this content, the author has been banned you.'), 'error');
    drupal_goto('node/' . $comment->nid);
    return FALSE;
  }
}

/**
 * Check if a user is banned
 */
function user_ban_exists($user_who_banned, $user_who_get_banned) {
  if (is_object($user_who_banned)) {
    $user_who_banned = $user_who_banned->uid;
  }
  if (is_object($user_who_get_banned)) {
    $user_who_get_banned = $user_who_get_banned->uid;
  }
  $flag = flag_get_flag('user_ban');
  $query = db_select('flagging')
          ->fields('flagging', array('flagging_id'))
          ->condition('fid', $flag->fid)
          ->condition('entity_id', $user_who_get_banned)
          ->condition('uid', $user_who_banned);
  return $query->countQuery()->execute()->fetchField();
}
