<?php

function user_ban_flag_default_flags() {
  $flags = array();
// Exported flag: "Ban user".
  $flags['user_ban'] = array(
      'entity_type' => 'user',
      'title' => t('Ban user'),
      'global' => 0,
      'types' => array(),
      'errors' => array(),
      'flag_short' => t('Ban'),
      'flag_long' => '',
      'flag_message' => '',
      'unflag_short' => t('Unban'),
      'unflag_long' => '',
      'unflag_message' => '',
      'unflag_denied_text' => '',
      'link_type' => 'normal',
      'weight' => 0,
      'show_in_links' => array(
          'full' => 0,
          'token' => 0,
      ),
      'show_as_field' => 0,
      'show_on_form' => 0,
      'access_author' => '',
      'show_contextual_link' => 0,
      'show_on_profile' => 0,
      'access_uid' => 'others',
      'module' => 'user_ban',
      'api_version' => 3,
  );
  return $flags;
}
